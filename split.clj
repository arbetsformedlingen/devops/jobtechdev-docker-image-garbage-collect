#!/usr/bin/env bb

(defn split-line [line]
  (let [[repo-with-image tag] (clojure.string/split line #":")
        [repo image] (clojure.string/split repo-with-image #"/" 2)]
    ;;(println "Repository:" repo)
    ;;(println "Image:" image)
    ;;(println "Tag:" tag)
    (println image)
    ))

(defn -main [& args]
  (loop [line (read-line)]
    (when line
      (do
        (split-line line)
        (recur (read-line))))))

(-main)
