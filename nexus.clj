(require '[babashka.curl :as curl])
(require '[cheshire.core :as json])

(defn fetch-url [url]
  (-> (curl/get url)
      :body
      ))

(def image "semantic-concept-search/semantic-concept-search")
(def rs (fetch-url (str "https://nexus.jobtechdev.se/service/rest/v1/search?docker.imageName=" image)))
;;(println "rs:" rs)


(def data (json/parse-string rs true) )
(def versions (map :version (get-in data [:items])))
(def images (map #(str "docker-images.jobtechdev.se/" image ":" % ) versions))

;;(println images)
(dorun (map #(println %)  images))

;;(println data)
;;(println (get-in data ["items" "continuationToken"]))  ; changes the path as per the JSON structure
