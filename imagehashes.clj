#!/usr/bin/env bb
;; cat all-uniq-images.txt |bb imagehashes.clj

(def image-hashes (atom {}))

(defn split-line [line]
  (let [[repo-with-image tag] (clojure.string/split line #":")
        [repo image] (clojure.string/split repo-with-image #"/" 2)]
    ;;(println "Repository:" repo)
    ;;(println "Image:" image)
    ;;(println "Tag:" tag)
    (reset! image-hashes (assoc @image-hashes image (conj (get-in @image-hashes [image])tag)))
    ))



(defn -main [& args]
  (loop [line (read-line)]
    (when line
      (do
        (split-line line)
        (recur (read-line)))))
  (println (prn-str @image-hashes))
  )

(-main)
