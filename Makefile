.ONESHELL:

SHELL = /bin/bash

include settings.mk

# * figure out images in use in our four clusters
# you need to be logged in to all clusters for this to work
testing-images.txt testing-cron-images.txt:
	oc config use-context default/api-testing-services-jtech-se:6443/$(OCPUSER)
	./clusterimages.sh testing

prod-images.txt prod-cron-images.txt:
	oc config use-context default/api-prod-services-jtech-se:6443/$(OCPUSER)
	./clusterimages.sh prod

onprem-testing-images.txt onprem-testing-cron-images.txt:
	oc config use-context default/api-joct4-arbetsformedlingen-se:6443/$(OCPUSER)
	./clusterimages.sh onprem-testing

onprem-prod-images.txt onprem-prod-cron-images.txt:
	oc config use-context default/api-jocp4-arbetsformedlingen-se:6443/$(OCPUSER)
	./clusterimages.sh onprem-prod

# * post process
# all unique images in use in all clusters
all-uniq-images.txt: prod-images.txt prod-cron-images.txt testing-images.txt testing-cron-images.txt onprem-prod-images.txt onprem-prod-cron-images.txt onprem-testing-images.txt onprem-testing-cron-images.txt
	cat testing-images.txt testing-cron-images.txt prod-images.txt prod-cron-images.txt onprem-prod-cron-images.txt onprem-testing-images.txt onprem-testing-cron-images.txt|sort|uniq >all-uniq-images.txt

# strip image hashes, so we just have images in use (not used atm)
all-uniq-images-just-images.txt: all-uniq-images.txt
	cat all-uniq-images.txt |./split.clj|uniq > all-uniq-images-just-images.txt

# edn format is better for bb
all-uniq-images.edn: all-uniq-images.txt
	cat all-uniq-images.txt |bb imagehashes.clj>all-uniq-images.edn

# these are for examining individual clusters, not used for the main report
prod-uniq-images.edn:prod-images.txt
	cat prod-images.txt|sort|uniq |bb imagehashes.clj>prod-uniq-images.edn

report-prod.txt prod-maybe-safe-to-delete.txt: prod-uniq-images.edn
	cat prod-uniq-images.edn |bb nexus2.clj --filename prod >report-prod.txt

testing-uniq-images.edn:testing-images.txt
	cat testing-images.txt|sort|uniq |bb imagehashes.clj>testing-uniq-images.edn

report-testing.txt testing-maybe-safe-to-delete.txt: testing-uniq-images.edn
	cat prod-uniq-images.edn |bb nexus2.clj --filename testing >report-testing.txt



# * reporting
# main report, which considers all our cluster
report.txt all-maybe-safe-to-delete.txt: all-uniq-images.edn
	cat all-uniq-images.edn |bb nexus2.clj --filename all >report.txt

# * main target
all: report.txt report-prod.txt report-testing.txt

# * cleaning
clean:
	rm *.txt all-uniq-images.edn testing-uniq-images.edn prod-uniq-images.edn    || true
