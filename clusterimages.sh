#!/usr/bin/env bash
cluster=$1
oc get pods --all-namespaces -o jsonpath="{..image}" | tr -s ' ' '\n' | grep docker-images.jobtechdev.se|sort|uniq > ${cluster}-images.txt
oc get cronjobs --all-namespaces -o jsonpath="{.items[*].spec.jobTemplate.spec.template.spec.containers[*].image}" | tr -s ' ' '\n' | grep docker-images.jobtechdev.se|uniq > ${cluster}-cron-images.txt
