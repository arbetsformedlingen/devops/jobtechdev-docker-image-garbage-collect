#!/usr/bin/env bb
(require '[babashka.process :as p]
         '[clojure.edn :as edn]
         '[babashka.fs :as fs])
(require '[babashka.cli :as cli])
(require '[clojure.string :as str])
(require '[com.grzm.awyeah.client.api :as aws])
(require '[taoensso.timbre :as timbre])


;; * config stuff
(defn parse-config [filename]
  (edn/read-string  (slurp filename)))
(def config (parse-config "settings.edn"))

;; * url encode decode

(defn urldecode [s]
  (java.net.URLDecoder/decode s "UTF-8"))

(defn urlencode [s]
  (java.net.URLEncoder/encode s "UTF-8"))


;; * docker stuff
(defn nexus-login []
  (timbre/info "docker login to nexus " (:user config))
  (-> (p/sh "docker" "login" "docker-images.jobtechdev.se" "-u" (:user config) "-p" (:password config))     :out     timbre/info))

(defn nexus-pull [img]
  (timbre/info "docker pull from nexus " img)
  (-> (p/sh "docker" "pull" img)     :out     timbre/info))

(defn remove-local-image [img]
  (timbre/info "docker rmi " img)
  (-> (p/sh "docker" "rmi" img)     :out     timbre/info))

(defn nexus-backup-image [img]
  (let [filename (str (urlencode img) ".tar")]
    (timbre/info "docker backup " filename img)
    (-> (p/sh "docker" "save" "-o" filename img)     :out     timbre/info)
    ;;aparently i have to verify the image file here
    ;; this can weirdly error out, if so, try to do something else than error out
    (if (not (fs/exists? filename)) (do (timbre/error "file " filename " wasnt produced!")
                                        (throw (Exception. " backup file wasnt produced for weird reasons "))) )
    filename
    ))


;;pull the image, store it in a tar file, remove the local image
(defn backup-image-from-nexus [img]
  (let [
        a (nexus-login)
        b (nexus-pull img)
        tarfile (nexus-backup-image img)
        d (remove-local-image img)]
    tarfile))


(defn restore-image-to-nexus [filename]
  (nexus-login)
  (timbre/info "restore " filename)
  (-> (p/sh "docker" "load" "-i" filename)     :out     timbre/info) ;; # docker load -i /path/to/your-image-name.tar
  (timbre/info "push to nexus ")
  
  (let [img (fs/strip-ext (urldecode filename))
        proc (p/process ["docker" "push" img] {:inherit true})] (p/check proc) ;; so progress happens on stdout
       (remove-local-image img)) ;; remove local image after done
  )

;; # restore
;; # docker load -i /path/to/your-image-name.tar
;; # docker tag your-image-name nexus-repository-url/your-image-name:tag
;; # docker push nexus-repository-url/your-image-name:tag



(defn nexus-get-docker-image-id [image user pwd]
  (let [split (str/split image #":")
        img (urlencode (first split))
        version (second split)
        url (str "https://nexus.jobtechdev.se/service/rest/v1/search?repository=JobtechdevDockerRegistry&version=" version "&docker.imageName=" img)]
    (timbre/info split img version url)
    (-> (curl/get url
                  {:user (str user ":" pwd)})
        :body
        (json/parse-string ,   true)
        (get-in , [:items 0 :id] )
        )))
;; bb dockerbackup.bb --id --image jobtech-annotation-editor/jobtech-annotation-editor:b801795
;; but not
;;bb dockerbackup.bb --id --image docker-images.jobtechdev.se/jobtech-annotation-editor/jobtech-annotation-editor:b801795


;; might need:
(defn split-docker-image-id [image-id]
  (let [parts (str/split image-id #"/")
        [repository image-tag] (if (> (count parts) 1) parts [nil (first parts)])
        [image tag] (if (str/includes? image-tag ":") (str/split image-tag #":") [image-tag nil])]
    {:repo repository, :image image, :tag tag}))

(defn split-docker-image-id-r [image-id]
  (let [[_ repo image tag] (re-matches #"(?:(.+)/)?([^:]+)(?::(.*))?" image-id)]
    {:repo repo, :image image, :tag tag}))


;;(def split-image-id (split-docker-image-id "docker-images.jobtechdev.se/jobtech-annotation-editor/jobtech-annotation-editor:b801795"))

;;remove
;; get the nexus component id for the container:
;; curl  -u 'verjo:xxx' -X 'GET'  'https://nexus.jobtechdev.se/service/rest/v1/search?repository=JobtechdevDockerRegistry&version=b801795&docker.imageName=jobtech-annotation-editor%2Fjobtech-annotation-editor'
;; delete the img id from nexus
;; curl -u 'verjo:xxx' -X DELETE  'https://nexus.jobtechdev.se/service/rest/v1/components/Sm9idGVjaGRldkRvY2tlclJlZ2lzdHJ5OjcxYWZlYTU0MGUyM2RkZTUzMWMyMzNkNDFlOWY4NGE2' 
;; the id will change anytime you restore


;; * aws stuff
(def s3 (aws/client {:api :s3}))

(defn list-buckets []
  (def buckets (-> (aws/invoke s3 {:op :ListBuckets})
                   :Buckets))
  (prn buckets))

(defn backup-image-tar [img-tar]
  (aws/invoke s3 {:op :PutObject :request {:Bucket (:bucket config) :Key img-tar
                                           :Body (io/input-stream  (io/file img-tar))}})
  (aws/invoke s3 {:op :ListObjectsV2 :request {:Bucket (:bucket config)}}))
;;bb dockerbackup.bb --backup-image-tar --filename docker-images.jobtechdev.se%2Fjobtech-annotation-editor%2Fjobtech-annotation-editor%3Ab801795.tar

(defn ls-bucket []
  (let [ls  (aws/invoke s3 {:op :ListObjectsV2 :request {:Bucket (:bucket config)}})]
    (doseq [s3file (get-in ls [:Contents])]
      (println (get-in s3file [:Key])))
    (println "number of files: " (count (get-in ls [:Contents])))
    )
  )

;; * iterate the image file

(defn split-and-move-lines [input-file output-file]
  (let [lines (line-seq (io/reader input-file))
        first-line (first lines)
        remaining-lines (str/join "\n" (rest lines))]
      (spit input-file remaining-lines)
      (spit output-file (str  first-line "\n") :append true)
        first-line)
  
  )

(defn iterate-image-file []
  (timbre/info "Process starting...")
  (loop [n 1]
    (let [image-line (first (line-seq (io/reader "all-maybe-safe-to-delete.txt")))]
      (timbre/info "---------------------------------------------------------------------------------------")
      (timbre/info "Processing line:" n  " : " image-line)
      (try
        (let [tarfile (backup-image-from-nexus image-line)]
          (backup-image-tar tarfile)        
          (fs/delete-if-exists tarfile)     
          ;;delete image from nexus, no, do it in a separate step, because the backup is error prone in unexpected ways
          ;; if all went well move the line we worked on to the new file
          (split-and-move-lines "all-maybe-safe-to-delete.txt" "all-maybe-safe-to-delete-moved.txt"))
        (catch Exception e
          (timbre/error "bad stuff happened during backup of image " image-line "  error:" (.getMessage e))
          ;; there is also the case that processing image-line failed, if so split it to a failed file
          (split-and-move-lines "all-maybe-safe-to-delete.txt" "all-maybe-safe-to-delete-failed.txt")))
      (if image-line (recur (inc n))
          (timbre/info "done")))))

;; * delete images succesfully backed up
;; backing up images to s3 failed unexpected ways, but mostly it worked
;; rather than trusting the file of processed images, iterate the s3 backups, and delete the corresponding image from nexus
;; i can do some sanity check of the backup as well, to verify it isnt corrupted in some way
(defn delete-backed-up-images []
  ;; TODO
  )


;; * main, help
(defn help []
(timbre/info "--backup or --restore"))

(defn -main [args]
(cond (:backup args)   (backup-image-from-nexus (:image args))
      (:restore args) (restore-image-to-nexus  (:filename args))
      (:getid args) (timbre/info (nexus-get-docker-image-id (:image args) (:user config)  (:password config) ))
;;      (:delete args) (timbre/info (nexus-rm-docker-image-id (:id args) (:user config)  (:password config) ))
      (:list-buckets args) (list-buckets)
      (:ls-bucket args) (ls-bucket)
      (:backup-image-tar args) (backup-image-tar (:filename args))
      (:iterate args) (iterate-image-file)
      (:trylog args) (do (timbre/info "info from timbre") (timbre/error "error from timbre") (timbre/debug "info from timbre") (timbre/trace "info from timbre"))
      :else (help))

)


(-main (cli/parse-opts *command-line-args* {:coerce {:image :string}}))

;;bb dockerbackup.bb --image docker-images.jobtechdev.se/jobtech-annotation-editor/jobtech-annotation-editor:b801795

