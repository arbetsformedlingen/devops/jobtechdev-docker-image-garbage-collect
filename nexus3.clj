(require '[babashka.curl :as curl])
(require '[cheshire.core :as json])

;;curl, but in clojure
(defn fetch-url [url]
  (-> (curl/get url)
      :body      
      ))

(defn fetch-url-with-retries-and-timeout
  [url]
  (let [max-retries 10
        timeout 5000]
    (loop [i 0]
      (let [result (let [f (future (fetch-url url))]
                     (try
                       (deref f timeout nil)
                       (catch java.util.concurrent.TimeoutException _
                         (println "Request timed out, retrying...")
                         (future-cancel f)
                         nil)))]
        (if (or result (>= i max-retries))
          (or result (do (println "All retries failed. Exiting...") (System/exit 1)))
          (do
            (println (str "Curling Nexus Attempt " (inc i) " failed. Retrying..."))
            (Thread/sleep 1000)  ; Wait for a second before retrying
            (recur (inc i))))))))


(defn my-fetch-url-with-continuations-and-retries [url]
  (let [
        ]
    (loop [i 0
           cont-token nil
           results []]  ; Initialize results as an empty list
      (let [url-with-continuation (if cont-token  (str url "&continuationToken=" cont-token ) url)
            result (json/parse-string  (fetch-url-with-retries-and-timeout url-with-continuation) true)

            continuationToken  (:continuationToken result)
            dbg (println i continuationToken (count (:items result)))
            ]
        (if   (not continuationToken)
          results
          (recur (inc i) continuationToken (concat results [result]))
          )
        
        ))))

(defn join-items [maps]
  (reduce (fn [acc m] (merge-with concat acc m)) {} maps))



;;(println (fetch-url-with-retries-and-timeout "https://nexus.jobtechdev.se/service/rest/v1/search?docker.imageName=education-frontend-app/education-frontend-app"))
;;(println (fetch-url-with-continuations-and-retries "https://nexus.jobtechdev.se/service/rest/v1/search?docker.imageName=education-frontend-app/education-frontend-app"))

(let [data (my-fetch-url-with-continuations-and-retries "https://nexus.jobtechdev.se/service/rest/v1/search?docker.imageName=education-frontend-app/education-frontend-app")
      join-data  (join-items data)
      items (get-in join-data [:items])
      versions (map :version items)
      ]
  (doseq [version versions](println version))

  )


