(require '[babashka.curl :as curl])
(require '[cheshire.core :as json])
(require '[clojure.java.io :as io])
(require '[babashka.cli :as cli])

;;curl, but in clojure
(defn fetch-url [url]
  (-> (curl/get url)
      :body
      ))

;;nexus started to hang randomly, so introduce this wrapper on fetch-url, that does retries
(defn fetch-url-with-retries-and-timeout
  [url]
  (let [max-retries 10
        timeout 5000]
    (loop [i 0]
      (let [result (let [f (future (fetch-url url))]
                     (try
                       (deref f timeout nil)
                       (catch java.util.concurrent.TimeoutException _
                         (println "Request timed out, retrying...")
                         (future-cancel f)
                         nil)))]
        (if (or result (>= i max-retries))
          (or result (do (println "All retries failed. Exiting...") (System/exit 1)))
          (do
            (println (str "Curling Nexus Attempt " (inc i) " failed. Retrying..."))
            (Thread/sleep 1000)  ; Wait for a second before retrying
            (recur (inc i))))))))


;;and also the nexus rest api is paginated, so need to manage continuation tokens
;; and this now returns parsed json, because issues with fetching the continuationtoken as string for some reason
(defn my-fetch-url-with-continuations-and-retries [url]
  (let [
        ]
    (loop [i 0
           cont-token nil
           results []]  ; Initialize results as an empty list
      (let [url-with-continuation (if cont-token  (str url "&continuationToken=" cont-token) url)
            result (json/parse-string  (fetch-url-with-retries-and-timeout url-with-continuation) true)
            continuationToken  (:continuationToken result)            
            ]
        (if   (not continuationToken)
          (do
            results)  ; Return the results list when done
          (do
            (recur (inc i) continuationToken (concat results [result]))))))))



;;read from stdin
(def image-hashes *input*)

(defn join-items [maps]
  (reduce (fn [acc m] (merge-with concat acc m)) {} maps))


;;given an image, return the versions nexus has for it
(defn nexus-image-versions [image]
  (let [query (str "https://nexus.jobtechdev.se/service/rest/v1/search?docker.imageName=" image)
        data  ( my-fetch-url-with-continuations-and-retries query)
        join-data (join-items data)
        ;;data  (json/parse-string rs true)
        items (get-in join-data [:items])
        ;;dbg (println items)
        versions (map :version items)
        ]
;;    (println "dbg query:" query)
;;    (println "dbg nexus versions:" versions)
    versions)
  )

(defn version-exists-in-nexus [version]
  (let [rv (fetch-url-with-retries-and-timeout (str "https://nexus.jobtechdev.se/service/rest/v1/search?version="version))
        data (json/parse-string rv true)
        exists (get-in data [:items 0 :id])
        ]
    exists)
  )

;;the main runner, iterate all the images in stdin, lookup the images in nexus, do some set arithmetic.
(defn -main [args]
  ;;(println image-hashes)
  (doseq [[image hashes] image-hashes]
    (let ;;[nexus-versions (doall (nexus-image-versions image))]
        [nexus-versions (nexus-image-versions image)
         in-use-minus-nexus (clojure.set/difference (set hashes) (set nexus-versions) )
         nexus-not-in-use (clojure.set/difference (set nexus-versions) (set hashes))  ;; these should be safe to delete, but might also contain false negatives(stuff that is actually safe to delete)
         safe-file (str (:filename args) "-maybe-safe-to-delete.txt")
         ]
      (println "--------------------------------------------------------")
      (println (str "image:" image "\n in-use hashes:" hashes))
      (println " nexus versions not in use :" nexus-not-in-use)
      (print " in use versions - nexus versions(non-empty is an error, probably nexus pagination bug) :" in-use-minus-nexus)
      (if (not (empty? in-use-minus-nexus))
        (println " ERROR!!! image: " image)
        (println " OK!"))
      (doseq [version in-use-minus-nexus] (println "version:" version " exists:" (version-exists-in-nexus version)))
      (with-open [w (io/writer safe-file :append true)]
        (doseq [safe-version nexus-not-in-use] (.write w  (str "docker-images.jobtechdev.se/" image ":" safe-version "\n")))
)
      
      )
    )
  
  )

(-main (cli/parse-opts *command-line-args* {:coerce {:filename :string}}))


